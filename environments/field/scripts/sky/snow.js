/*
snow.js will create snowfall in the background of the simulator
Each snowflake has its own density so they fall at different paces
The snow fall sway to the left or right to give it a sense of "wind"
*/


//snowflake particles
var W = canvas.width;
var H = canvas.height;
var mp = 25; //max particles

if(tempDataStorage_back == undefined)
{

	tempDataStorage_back = {"angle": 0, "particles" : []};
	var particles = tempDataStorage_back.particles;
	for(var i = 0; i < mp; i++)
	{
		particles.push({
			x: Math.random()*W, //x-coordinate
			y: Math.random()*H, //y-coordinate
			r: Math.random()*4+1, //radius
			d: Math.random()*mp //density
		})
	}
	
}
else
{
	var particles = tempDataStorage_back.particles;
	//console.log(particles);
}


//Update their information for the next call		
tempDataStorage_back.angle += 0.01;
var angle = tempDataStorage_back.angle;
for(var i = 0; i < mp; i++)
{
	var p = particles[i];
	//Updating X and Y coordinates
	//Adding 1 to the cos function to prevent negative values which will lead flakes to move upwards
	//Get rid of adding 1 to defy the laws of physics
	
	//Every particle has its own density which can be used to make the downward movement different for each flake
	//make it more random by adding in the radius
	p.y += Math.cos(angle+p.d) + 1 + p.r/2;
	p.x += Math.sin(angle) * 2;
	
	//Sending flakes back from the top when it exits
	//let flakes enter from the left and right also.
	if(p.x > W+5 || p.x < -5 || p.y > H)
	{
		if(i%3 > 0) //66.67% of the flakes
		{
			particles[i] = {x: Math.random()*W, y: -10, r: p.r, d: p.d};
		}
		else
		{
			//If the flake is exitting from the right
			if(Math.sin(angle) > 0)
			{
				//Enter from the left
				particles[i] = {x: -5, y: Math.random()*H, r: p.r, d: p.d};
			}
			else
			{
				//Enter from the right
				particles[i] = {x: W+5, y: Math.random()*H, r: p.r, d: p.d};
			}
		}
	}
}

//Lets draw the flakes
ctx.clearRect(0, 0, W, H);
ctx.fillStyle = "black";
ctx.fillRect(0, 0, W, H);

ctx.fillStyle = "rgba(255, 255, 255, 0.8)";
ctx.beginPath();
for(var i = 0; i < mp; i++)
{
	var p = particles[i];
	ctx.moveTo(p.x, p.y);
	ctx.arc(p.x, p.y, p.r, 0, Math.PI*2, true);
}
ctx.fill();
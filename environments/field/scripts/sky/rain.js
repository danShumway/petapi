/*
rain.js creates rainfall for the background.

An array holds each character that represents a droplet where
each droplet will descend at a different rate

*/


var character = "|";
character = character.split("");
var c = canvas;
var font_size = 10;
var columns = c.width/font_size; //number of columns for the rain

if(tempDataStorage_back == undefined)
{

	//an array of drops - one per column
	tempDataStorage_back = [];
	var drops = tempDataStorage_back;
	//x below is the x coordinate
	//1 = y co-ordinate of the drop(same for every drop initially)
	for(var x = 0; x < columns; x++)
		drops[x] = Math.random() * c.height; 
		
}
else
{
	drops = tempDataStorage_back;
}

//Black BG for the canvas
//translucent BG to show trail

canvas.style.background = "rgba(144,144,144, 0.05)";
ctx.clearRect(0,0, c.width, c.height);
ctx.fillStyle = "rgba(144,144,144, 0.05)";
ctx.fillRect(0, 0, c.width, c.height);

ctx.fillStyle = "#06F"; //blue text
ctx.font = font_size + "px arial";
//looping over drops
for(var i = 0; i < drops.length; i++)
{
	//a random character to print
	var text = character[Math.floor(Math.random()*character.length)];
	//x = i*font_size, y = value of drops[i]*font_size
	ctx.fillText(text, i*font_size, drops[i]*font_size);
	
	
	//sending the drop back to the top randomly after it has crossed the screen
	//adding a randomness to the reset to make the drops scattered on the Y axis
	if(drops[i]*font_size > c.height && Math.random() > 0.975)
		drops[i] = 0;
	
	//incrementing Y coordinate
	drops[i]++;
}


	//setInterval(draw, 33);
	/*
	for (var i = 0; i < 10000; i+=33)
	{
		draw();
	}
	*/

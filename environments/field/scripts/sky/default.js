/*
default.js will create a sunny background as our default background

The sun is comprised of a center circle surrounded with sun rays and spins
*/

if(tempDataStorage_back == undefined)
{
	tempDataStorage_back = new Object({
	   canvas: false,
	   context: false,
	   //amount of time before it will call itself again for the animation loop
	   interval: false,
	   //the rate at which the sun will rotate at
	   offset: 0,
	   //we will call the ray object's init on load to set up the sun animation
	   init: function(id, color1, color2){
		  this.canvas = document.getElementById(id);
		  this.context = ctx;
		  canvas.style.background = color1;
		  this.backgroundColor = color1;
		  this.sunColor = color2;
		  clearInterval(this.interval);
		  /*this.interval = setInterval(function(){
			 rays.offset += 0.005;
			 rays.draw();
		  }, 10);*/
		  this.draw();
	   },
	   //pass in midpoint coordinates, the length, and angle (this is in radians btw)
	   //returns an object with x/y coordinates that we will use later to draw the sun-rays
	   getXY: function(x, y, d, a){
		  return {
			 x: x + d * Math.cos(a),
			 y: y + d * Math.sin(a)
		  };
	   },
	   draw: function(){
		  c = canvas;
		  //this.context.clearRect(0, 0, c.width, c.height);
		  this.context.fillStyle = this.backgroundColor;
		  this.context.fillRect(0,0,c.width,c.height);
		  this.context.fillStyle = this.sunColor;
		  //make sure the rays are bigger than the canvas
		  length = Math.max(c.width, c.height);
		  //get the mid point of the canvas
		  midx = c.width / 2, midy = c.height / 2;
	 
		  //going to start off with 8 rays
		  var d = 8;
		  for(i = 0;i < d;i++){
			 angle = (Math.PI * 2 / d) * i + this.offset;
			 this.offset += 0.005;
			 //move to the center of the canvas
			 this.context.moveTo(midx, midy);
			 //calculate the first coordinate
			 c1 = this.getXY(midx, midy, length, angle + d / 100);
			 //draw the line to the first coordinate in the pair that make up the "end points" of the ray
			 this.context.lineTo(c1.x, c1.y);
			 //calculate the second coordinate
			 c2 = this.getXY(midx, midy, length, angle - d / 100);
			 //draw the line to the second coordinate
			 this.context.lineTo(c2.x, c2.y);
			 //line back to the middle
			 this.context.lineTo(midx, midy);
			 this.context.fill();
		  };
		  
		  //the "sun"
		  this.context.beginPath();
		  this.context.arc(midx, midy, c.width * c.height / 20000,
			 0, Math.PI * 2, true);
		  this.context.closePath();
		  this.context.fill();
	   }
	});

	//canvas.height = window.innerHeight;
	//canvas.width = window.innerWidth;
	tempDataStorage_back.init('c',  'rgb(255, 215, 0)', 'rgb(255, 255, 0)');

}

tempDataStorage_back.offset += 0.005;
tempDataStorage_back.draw();
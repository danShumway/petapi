//You will have access to canvas, ctx, and a number of other things, including dt. 
//Don't put a ctx.clearRect, the main program will handle that.
//You have canvas
//You have ctx

//timeofday
//

ctx.fillStyle = "green";

for(var i = 0; i < canvas.width; i++)
{
	//toTrace = toTrace + Math.sin(2 * Math.PI / canvas.width * (i/2) + Math.PI) + canvas.height/2 + "/n"; 
	ctx.fillRect(i, (Math.sin(2 * Math.PI / canvas.width * (i/2) + Math.PI) * 60) + canvas.height/1.5, 1, canvas.height);
}

//Add some flowers if localStorage isn't defined.
//yes, we're using == here, because I want the check to be generous.
if(tempDataStorage_fore == undefined)
{
	tempDataStorage_fore = new Array();
	for(i = 0; i < 15; i++)
	{
		tempDataStorage_fore.push({
			"x" : Math.random()*canvas.width, 
			"y" : Math.random()*(canvas.height - canvas.height/1.5) + canvas.height/1.5,
			"color" : 'rgb(' + Math.floor(Math.random()*255) + ',' + Math.floor(Math.random()*255) + ',' + Math.floor(Math.random()*255) + ')'});
	}

	console.log('data created');
}
//Otherwise, just use the ones that are defined.
else
{
	for(i = 0; i < 15; i++)
	{
		ctx.fillStyle = tempDataStorage_fore[i].color;
		ctx.beginPath();
		ctx.arc(tempDataStorage_fore[i].x, tempDataStorage_fore[i].y, 4, 0, 2*Math.PI);
		ctx.fill();
	}
}
//scriptStorage

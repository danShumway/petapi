//-----------------------------------------------------------------
/*
You'll note that absent from here is the voice recognition,
which is required in the main page.

This is because voice recognition is used more than once, and because 
I want the API to be even easier to swap out than any of the other things that
are being used, because I don't like that voice recognition is chrome only.

It's also somewhat tied into the interface, which I don't want to be controlled
from here too much.

Otherwise, this is just a way to make loading the puppy easier for the first time.

We use Modernizr in here (http://modernizr.com/) because it's awesome.
*/
//--------------------------------------------------------------------
function MakePetContext()
{
	//Init. This part is easy.
	var toReturn = {
		"pet" : Animal(),
		"environment" : Environment(),
		"currentWeather" : undefined
	};

	//-------------------------------------------------
	/*Time to set everything else up.*/
	//-------------------------------------------------


	//Check for local storage and pull in data from there.
	if (Modernizr.localstorage)
	{
		//use parseInt to convert to numbers.
		var lastCheck = localStorage.getItem("lastCheck");
		//Get the current time.
		var lastTime = new Date();
		//There's a bug here, where if you waited 24 hours exactly, it wouldn't recheck.
		//Logged, but not fixed.
		if(lastCheck == undefined || parseInt(lastCheck) != lastTime.getHours()) //Weather is refreshed once every hour.
		{
			console.log('no local data found, rechecking weather site');
			//Update local data.
			localStorage.setItem("lastCheck", lastTime.getHours());

			//--------------------------------------
			/*check weather here*/
			//---------------------------------------
			var site = "http://api.openweathermap.org/data/2.5/weather";

				//If you can't get access to the GPS, use Rochester as the default lat and long.
				//--------------------------------------
				/*GPS*/
				//--------------------------------------
				if(navigator.geolocation)
				{
					navigator.geolocation.getCurrentPosition(
						function(position) {
							//I want == to be generous, so I'm not using ===
							if(position.coords.latitude == undefined || position.coords.longitude == undefined)
							{	//Use default.
								site = site + "?" + "q=" + "Rochester, NY";
								getWeatherData(site);
							}
							else
							{
								//Don't use default.
								site = site + "?" + "lat=" + position.coords.latitude + "&lon=" + position.coords.longitude;
								getWeatherData(site);
							}

						});
				}
				else
				{
					//Use default.
					site = site + "?" + "q=" + "Rochester, NY";
					getWeatherData(site);

				}
				//---------------------------------------
				/*End of GPS*/
				//---------------------------------------

			//--------------------------------------
			/*End of weather check.*/
			//--------------------------------------

		}
		else
		{
			console.log('weather checked within the last hour, not rechecking.');
			toReturn.currentWeather = JSON.parse(localStorage.getItem("currentWeather"));
		}
	}
	else
	{
		//Let the user know what's going on.
		//You'd probably want to change this line to an alert
		//in your own app.
		console.log("Your browser does not support local storage.  No puppies for you.");
		return false; //Puppy could not be created.
	}


	return toReturn;


	//Purely here for organizational purposes.
	function getWeatherData(thingToUse)
	{

		$.ajax({
			//You will need a key from OpenWeather.API
			url: thingToUse + "&APPID=INSERT_YOUR_API_KEY_HERE",
			dataType: 'jsonp',
			success : function(parsed_json)
			{
				console.log("Got weather data: " + parsed_json);
				toReturn.currentWeather = parsed_json;
				localStorage.setItem("currentWeather", JSON.stringify(parsed_json));
			}
		});
	}

}
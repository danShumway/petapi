//-------------------------API INFORMATION------------------------
/*
Flesh this out later.
*/
//---------------------------------------------------------------

function Environment(directory)
{

	var currentTime;
	function CurrentTime() { return currentTime; }
	//
	var directory; //What the directory for your data is.
	var currentStatus = "loading"; //Loading, ready, etc...
	function getCurrentStatus() { return currentStatus; }
	var states; //JSON for all of the possible world states.
	var currentState; //JSON element of the current state..  We're using it for weather, but you could do other things.
	var curBackgroundScript; //current Script to use for background.
	var curForegroundScript; //current Script to use for foreground.
	var tempDataStorage_back = undefined;
	var tempDataStorage_fore = undefined; //this storage will be preserved for as long as a script is running.
	//After which it will be cleared.

//-----------SETTERS-----------------------------
//-----------------------------------------------


	//We're using this for weather, but you could do other things with it.
	function setWorldState(state)
	{
		//Load the states, if you need to.
		currentState = states[state]; 
		if(currentState === undefined)
		{
			//go to default if you have to.
			currentState = states["default"];
			console.log('weather not found, reverting to default');
		}

		console.log(state);

		//Load the stuff in.
		$.ajax({
	      url: 'environments/' + directory + '/scripts/' + currentState["foregroundScript"],
	      success: function(data){
	      	console.log('script loaded');
	      	data.replace(/<br\s*\/?>/mg,"\n");
	    	tempDataStorage_fore = undefined;
	         eval("curForegroundScript = function(canvas, ctx, dt) {" + data + "};");
	         //alert(curForegroundScript);
	      },
	      dataType: "text"
	    });

	    $.ajax({
	      url: 'environments/' + directory + '/scripts/' + currentState["backgroundScript"],
	      success: function(data){
	      	console.log('script loaded');
	      	data.replace(/<br\s*\/?>/mg,"\n");
	      	tempDataStorage_back = undefined;
	         eval("curBackgroundScript = function(canvas, ctx, dt) {" + data + "};");
	         //alert(curForegroundScript);
	      },
	      dataType: "text"
	    });
	}

	//
	function setTimeOfDay(time)
	{
		//
		currentTime = time;
	}

//------------LOADING DATA-------------------------
/*

*/
//-------------------------------------------------

	function loadData(_directory)
	{
		directory = _directory;

		console.log("loading environment data");
		$.getJSON(
			"environments/" + directory + "/states.json",
			function(obj) {
				states = obj;
				currentStatus = "ready";
				console.log("environment data loaded");
			}
		);
	};

//-----------DRAW CODE----------------------------------
/*
drawBackground and drawForeground are private (maybe not in the future)
call draw instead.  Handles drawing, has a default script
in case none is provided.
*/
//--------------------------------------------------------

	function drawBackground(canvas, ctx, dt)
	{
		if(curBackgroundScript && curBackgroundScript != undefined)
		{
			
			curBackgroundScript(canvas, ctx, dt);
			
		}
		else
		{ //Use a default script instead.
			ctx.fillStyle = "blue";
			ctx.fillRect(0, 0, canvas.width, canvas.height);
		}
	}

	function drawForeground(canvas, ctx, dt)
	{
		if(curForegroundScript && curForegroundScript != undefined)
		{
			curForegroundScript(canvas, ctx, dt);
		}
		else
		{ //Use a default script instead.
			ctx.fillStyle = "green";
			ctx.fillRect(0, canvas.height/1.45, canvas.width, canvas.height);
		}
	}

	function draw(canvas, ctx, dt)
	{
		drawBackground(canvas, ctx, dt);
		drawForeground(canvas, ctx, dt);
	}



	//----------------------RETURN----------------------------------

	var toReturn = 
	{
		"CurrentTime" : CurrentTime,
		"setWorldState" : setWorldState,
		"setTimeOfDay" : setTimeOfDay,
		"draw" : draw,
		"loadData" : loadData,
		"getCurrentStatus" : getCurrentStatus
	}

	return toReturn;
}
//Written by Daniel Shumway - 11/2013
//Licensed under Creative Commons 0.
//We use closures for this because we plan on open sourcing it
//Proper encapsulation seems pretty important if people want to branch out and make
//other things with it.
function Animal()
{

	//getters and setters
	var currentState = "loading";
	function getCurrentState() { return currentState; }
	var image;
	var curX, curY, width, height = 0;
	var animationTimer = 0; //Used internally.
	//Returns the frame to draw.
	function getImage() { var toReturn = { 
		"image" : image, 
		"x" : currentAnimation["frames"][currentFrame].position[0], 
		"y" : currentAnimation["frames"][currentFrame].position[1], 
		"width" : currentAnimation["frames"][currentFrame].size[0], 
		"height" : currentAnimation["frames"][currentFrame].size[1]
		}; 

		return toReturn;
	};



	//-----internal data------------
	var commands;
	var animations;
	var currentAnimation; //What animation you're currently on.
	var currentFrame = 0;
	//Get your pet's data.
	function loadData(directory) 
	{ 
		$.getJSON(
			"animals/" + directory + "/scripts/commands.json",
			function(obj) { 
				commands = obj;
				//load in the next set of data.
				$.getJSON(
					"animals/" + directory + "/scripts/animations.json",
					function(obj) { 
						animations = obj; 
						//Finish all the other initializations.
						//Set up animations.
						image = new Image();
						currentAnimation = animations["sleeping"];
						animationTimer = currentAnimation.frames[0].duration;
						//Set your puppy up in the default position.
						image.src =  "animals/" + directory + "/sprites/" + currentAnimation.file; 
						currentState = "ready";
					}
				);
			}
		);	
	};

	//At the moment, animals can only interprate single word commands.
	//I'll be working on a fix for that sometime.
	function sendCommand(command, logData)
	{
		var words = command.split(" ");
		var gotCommand = false;
		var heard = 0;
		var interprated = 0;
		//console.log(command);

		for(var i = 0; i < words.length; i++)
		{
			for(var j=0; j < commands["commands"].length; j++)
			{
				for(var k = 0; k < commands["commands"][j]["trigger"].length; k++)
				{
					if(words[i] == commands["commands"][j]["trigger"][k])
					{
						gotCommand = true;
						heard = j;
						interprated = k;
					}
				}
			}

			//Log data if required.
			if(logData)
				console.log("heard : " + command, "interprated : " + 
					((gotCommand) ? (commands["commands"][heard]["trigger"][interprated]) : "not interprated")
					);


			//Respond to the command.
			if(!gotCommand)
				return sendCommand("default_reaction", context);
			else {
				//Adjust animation.
				currentAnimation = animations[(commands["commands"][heard]["animation"])];
				currentFrame = 0;
				//[NOT IMPLEMENTED YET]
				//Return the command.
				return commands["commands"][heard]["response"];
			}
		}
	}

	function update(dt)
	{
		//Change the animation frames and other stuff accordingly.
		//I don't approve of separating ai and draw code when they're coupled so closely.
		//console.log("I got: " + dt);

		animationTimer -= dt;

		if(animationTimer <= 0)
		{
			currentFrame = (currentFrame + 1) % currentAnimation["frames"].length;
			animationTimer = currentAnimation["frames"][currentFrame].duration;
		}

	}



	var toReturn = {
		"getImage" : getImage,
		"loadData" : loadData,
		"sendCommand" : sendCommand,
		"update" : update,
		"getCurrentState" : getCurrentState
	}

	return toReturn;
}